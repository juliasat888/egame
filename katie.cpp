#include "MatrixReader.h"

/*
* Класс операций над матрицей
*/
class Matrix {

private:
	int** matrix;			/// Собственно матрица
	int size;				/// Размер матрицы
	MatrixReader* reader;	/// Сканер для ввода матрицы
	/*
	* Возвращает диагональную матрицу из текущей
	*/
	int** diagonalMatrix();
	/*!Умножить вектор на константу и результат записать в выходной вектор
	\param[in] vector вектор
	\param[in] length длина вектора
	\param[in] constant константа, на которую надо умножить строку
	*/
	int* multipleVector(int* vector, int length, int constant);
	/*!Прибавить к одному вектору другой
	\param[in] vector1 вектор, к которому нужно прибавить vector2
	\param[in] vector2 вектор, который нужно прибавить
	\param[in] length длина вектора
	*/
	int* sumOfTwoVectors(int* vector1, int* vector2, const int length);
public:
	/*
	* Конструктор
	*/
	Matrix();
	/*
	* Деструктор
	*/
	~Matrix();
	/*
	* Получить детерминант
	*/
	int getDeterminant();
};

Matrix::Matrix(){
	reader = new MatrixReader();
	this->size = reader->getMatrixSize();
	this->matrix = reader->getMatrix();
}

Matrix::~Matrix(){
}

int Matrix::getDeterminant(){
	int sorts=0;
	float determinant=0.0;
	int** diag = diagonalMatrix();
	determinant=diag[0][0];
	for(int i=1;i<this->size;i++)
		determinant=determinant*diag[i][i];
	return determinant;
}

int** Matrix::diagonalMatrix(){
	if(this->size<=0)
		return NULL;
	int* bufVector = new int[this->size];
	int** output = new int*[this->size];
	for(int row=0;row<this->size;row++)
	{
		output[row] = new int[this->size];
		if(matrix[row][row]!=0)
		{
			for(int submatrix=row+1;submatrix<this->size;submatrix++)
			{
				int constant=matrix[submatrix][row]/matrix[row][row];
				if(constant>0&&matrix[submatrix][row]*matrix[row][row]>0||constant<0&&matrix[submatrix][row]*matrix[row][row]<0)
					constant*=-1;
				if(constant)
				{
					bufVector = multipleVector(matrix[row],this->size,constant);
					output[row] = sumOfTwoVectors(matrix[submatrix],bufVector,this->size);
				}
			}
		}
	}
	for(int row=this->size-1;row>=0;row--)
	{
		if(matrix[row][row]!=0)
		{
			for(int submatrix=row-1;submatrix>=0;submatrix--)
			{
				int constant=matrix[submatrix][row]/matrix[row][row];
				if(constant>0&&matrix[submatrix][row]*matrix[row][row]>0||constant<0&&matrix[submatrix][row]*matrix[row][row]<0)
					constant*=-1;
				if(constant)
				{
					bufVector = multipleVector(matrix[row],this->size,constant);
					output[row] = sumOfTwoVectors(matrix[submatrix],bufVector,this->size);
				}
			}
		}
	}
	return output;
}

int* Matrix::multipleVector(int* vector, int length, int constant){
	if(length<=0)
		return NULL;
	int* vec = new int [length];
	for(int i=0;i<length;i++){
		vec[i]=vector[i]*constant;
	}
	return vec;
}

int* Matrix::sumOfTwoVectors(int* vector1, int* vector2, const int length){
	if(length<=0)
		return NULL;
	int* vec = new int [length];
	for(int i=0;i<length;i++){
		vec[i] = vector1[i]+vector2[i];
	}
	return vec;
}

int mul(int a, int b){
	return a*b;
}